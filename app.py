from flask import Flask, request

app = Flask(__name__)

@app.route('/query', methods=["GET"])
def query_example():
    Fruta = request.args.get('Fruta')
    return '''<h1>O nome da Fruta é: {}</h1>'''.format(Fruta),200

if __name__ == '__main__':
    app.run(port=5000)
